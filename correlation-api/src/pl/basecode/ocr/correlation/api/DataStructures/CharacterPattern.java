package pl.basecode.ocr.correlation.api.DataStructures;

public class CharacterPattern {
    private String character;
    private double[] vector;
    private int width;
    private int height;

    public String getCharacter() {
        return character;
    }

    public double[] getVector() {
        return vector;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public CharacterPattern(String character, double[] vector, int width, int height) {

        this.character = character;
        this.vector = vector;
        this.width = width;
        this.height = height;
    }
}

