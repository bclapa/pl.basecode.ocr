package pl.basecode.ocr.correlation.api.Patterns;

import pl.basecode.ocr.correlation.api.DataStructures.CharacterPattern;

import java.util.List;

public interface PatternProvider {
    List<CharacterPattern> getCharacterPatterns();
}
