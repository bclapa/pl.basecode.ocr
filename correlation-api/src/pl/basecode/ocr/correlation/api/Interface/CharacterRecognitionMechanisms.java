package pl.basecode.ocr.correlation.api.Interface;

import org.opencv.core.Mat;
import pl.basecode.ocr.correlation.api.DataStructures.LetterData;

/**
 * Created by bclapa on 23.03.2017.
 */
public interface CharacterRecognitionMechanisms {
    String recogniseUsingCorrelation(Mat letter);
    String recogniseTextLineCharactersUsingCorrelation(LetterData[] textLine, Mat image);
}

