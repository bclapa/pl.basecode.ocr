package pl.basecode.ocr.correlation.api.Interface;

import org.opencv.core.Mat;
import org.opencv.core.Rect;
import pl.basecode.ocr.correlation.api.DataStructures.LetterData;
import pl.basecode.ocr.correlation.api.DataStructures.TextLineData;

import java.util.List;

/**
 * Created by bclapa on 18.03.2017.
 */

public interface ImageContentOperations {
    Mat binarizeImageAndGetXAxisHistogram(Mat colorImage);
    Mat detectTextLinesAndGetLinesHistogram(Mat colorImage, int xMinimalValue);
    List<TextLineData> detectTextLines(Mat colorImage, int minimalValue);
    List<LetterData[]> detectLetterLocations(Mat colorImage, int xMinimalValue, int yMinimalValue, boolean insertWhitespaces);

    Rect findRactangularContours(Mat img);
    Mat trimToContours(Mat img);
}