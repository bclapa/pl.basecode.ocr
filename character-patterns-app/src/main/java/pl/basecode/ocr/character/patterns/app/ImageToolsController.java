package pl.basecode.ocr.character.patterns.app;

import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.opencv.core.Mat;
import pl.basecode.ocr.correlation.api.DefaultBasicImageOperations;
import pl.basecode.ocr.correlation.api.DefaultImageContentOperations;
import pl.basecode.ocr.correlation.api.Interface.BasicImageOperations;
import pl.basecode.ocr.correlation.api.Interface.ImageContentOperations;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageToolsController {
    @FXML
    private Pane rootNode;

    String directoryPath;
    BasicImageOperations imageOperations;
    ImageContentOperations imageContentOperations;

    public ImageToolsController() {
        imageOperations = new DefaultBasicImageOperations();
        imageContentOperations = new DefaultImageContentOperations(imageOperations);
    }

    @FXML
    protected void onChooseFolderClicked(MouseEvent e) {
        DirectoryChooser chooser = new DirectoryChooser();

        File file = chooser.showDialog(rootNode.getScene().getWindow());
        if (file == null)
            return;

        directoryPath = file.getPath();
    }

    @FXML
    protected void onProcessClicked(MouseEvent e) {
        try {
            xmlizeImagesInPath(directoryPath);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    protected void binarizeImagesInPath(String path) {
        File folder = new File(path);

        for (File file : folder.listFiles()) {

            if (file.isDirectory()) {
                binarizeImagesInPath(file.getPath());
                continue;
            }

            Mat img = imageOperations.loadImage(file.getAbsolutePath());
            img = imageOperations.binarizeColorImage(img);
            img = imageContentOperations.trimToContours(img);
            int index = file.getName().lastIndexOf('.');
            String extension = file.getName().substring(index, file.getName().length());

            String filePath = file.getAbsolutePath();

            byte[] data = new byte[img.width() * img.height() * (int)img.elemSize()];

            img.get(0,0,data);
            imageOperations.saveImage(img, extension.replace(".", ""), filePath);
        }
    }

    protected void xmlizeImagesInPath(String path) throws IOException {
        File folder = new File(path);

        Element patterns = new Element("patterns");
        Document doc = new Document(patterns);

        for (File file : folder.listFiles()) {

            if (file.isDirectory()) {
                xmlizeImagesInPath(file.getPath());
                continue;
            }

            Mat img = imageOperations.loadImage(file.getAbsolutePath());
            img = imageOperations.binarizeColorImage(img);
            img = imageContentOperations.trimToContours(img);

            patterns.addContent(convertToXmlElement(img, folder.getPath()));
        }

        XMLOutputter xmlOutput = new XMLOutputter();

        xmlOutput.setFormat(Format.getPrettyFormat());
        xmlOutput.output(doc, new FileWriter(folder.getAbsolutePath() + "\\patterns"+ folder.getName() +".xml"));
    }

    protected Element convertToXmlElement(Mat img, String pathName){
        Element pattern = new Element("pattern");

        byte[] data = new byte[img.width() * img.height() * (int)img.elemSize()];
        List<String> list = new ArrayList<>(data.length);

        img.get(0,0,data);

        for (byte b : data){
            list.add(Byte.toString((byte) (b+1)));
        }

        String concat = String.join(";", list);

        pattern.setAttribute("width",Integer.toString(img.width()));
        pattern.setAttribute("height",Integer.toString(img.height()));
        pattern.setAttribute("name",pathName);

        pattern.addContent(concat);

        return pattern;
    }
}
