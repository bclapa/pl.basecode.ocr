package pl.basecode.ocr.character.patterns.app;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import pl.basecode.ocr.correlation.api.DataStructures.LetterData;
import pl.basecode.ocr.correlation.api.DefaultBasicImageOperations;
import pl.basecode.ocr.correlation.api.DefaultCharacterRecognitionMechanisms;
import pl.basecode.ocr.correlation.api.DefaultImageContentOperations;
import pl.basecode.ocr.correlation.api.Interface.BasicImageOperations;
import pl.basecode.ocr.correlation.api.Interface.CharacterRecognitionMechanisms;
import pl.basecode.ocr.correlation.api.Interface.ImageContentOperations;
import pl.basecode.ocr.correlation.api.Patterns.DefaultPatternProvider;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainWindowController {
    @FXML
    private Pane rootNode;
    @FXML
    private Button loadButton;
    @FXML
    private Button processButton;
    @FXML
    private ImageView sourceImageView;
    @FXML
    private ImageView binarizedImageView;
    @FXML
    private ImageView histogramXAxisImageView;
    @FXML
    private ImageView histogramYAxisImageView;

    BufferedImage sourceImage;
    BufferedImage binarizedImage;
    BufferedImage xAxisHistogramImage;
    BufferedImage yAxisHistogramImage;

    BasicImageOperations imageOperations;
    ImageContentOperations imageContentOperations;
    CharacterRecognitionMechanisms characterRecognitionMechanisms;

    String imagePath;

    public MainWindowController() {
        imageOperations = new DefaultBasicImageOperations();
        imageContentOperations = new DefaultImageContentOperations(imageOperations);
        characterRecognitionMechanisms = new DefaultCharacterRecognitionMechanisms(imageOperations, imageContentOperations, new DefaultPatternProvider(imageOperations));
    }

    @FXML
    protected void loadButtonClicked(MouseEvent e) throws IOException {

        DirectoryChooser chooser = new DirectoryChooser();

        File file = chooser.showDialog(rootNode.getScene().getWindow());
        if (file == null)
            return;

        imagePath = file.getPath();
    }

    @FXML
    protected void processButtonClicked(MouseEvent e) throws IOException {

        int a = 0;

        for (File file : new File(imagePath).listFiles()) {
            if (file.isDirectory())
                continue;

            sourceImage = ImageIO.read(file);

            Mat image = imageOperations.convertImageToMat(sourceImage);
            Mat binarized = imageOperations.binarizeColorImage(image);
            binarizedImage = imageOperations.convertMatToImage(binarized);

            Image img = SwingFXUtils.toFXImage(binarizedImage, null);
            binarizedImageView.setImage(img);

            List<LetterData[]> letterData = imageContentOperations.detectLetterLocations(image, 0, 0, false);

            if (letterData.isEmpty()) {
                continue;
            }

            String outputDirectoryPath = imagePath + "/output";
            new File(outputDirectoryPath).mkdir();

            for (int i = 0; i < letterData.size(); i++) {
                LetterData[] row = letterData.get(i);
                int rowLength = row.length;

                for (int j = 0; j < rowLength; j++) {
                    LetterData letter = row[j];

                    String path = String.format(outputDirectoryPath + "/%0$d.bmp", a++);
                    Mat subImage = binarized.submat(new Rect(letter.getX(), letter.getY(), letter.getWidth(), letter.getHeight()));
                    //subImage = imageContentOperations.trimToContours(subImage);
                    imageOperations.saveImage(subImage, "bmp", path);
                }
            }
        }
    }

    @FXML
    protected void toolsButtonClicked(MouseEvent mouseEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/ImageTools.fxml"));

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }
}
