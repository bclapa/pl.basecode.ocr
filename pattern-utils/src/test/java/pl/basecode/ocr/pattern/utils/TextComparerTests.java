package pl.basecode.ocr.pattern.utils;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class TextComparerTests {
    @Test
    public void testAreEqual() {
        String pattern = "AlaMaKota";
        String text = "AlaMaKota";

        boolean areEqual = TextComparer.areEqual(pattern,text,true,true,false);

        Assert.assertTrue(areEqual);
    }

    @Test
    public void testGetResemblanceFactorForLine(){
        String pattern = "AlaMaKota";
        String text = "AlaMaKota";

        BigDecimal factor = TextComparer.getResemblanceFactorForLine(pattern,text,true,true,false);

        Assert.assertEquals(new BigDecimal(1), factor);
    }

    @Test
    public void testGetResemblanceFactorForText(){
        String[] patterns = new String[]{"Ala ma kota", "Kot ma Alę"};
        String[] text = new String[]{"Alamakota", "KotmaAlę"};

        BigDecimal factor = TextComparer.getResemblanceFactorForText(patterns,text, false, true, true);

        Assert.assertEquals(new BigDecimal(1), factor);
    }
}
