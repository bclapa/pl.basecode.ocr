package pl.basecode.ocr.pattern.utils;

import org.junit.Assert;
import org.junit.Test;

public class StringSplitTests {
    @Test
    public void should_split_when_unix_line_ending() {
        String pattern = "(\\r\\n)|(\\n)";
        String text = "line1\nline2\nline3\n";

        String[] strings = text.split(pattern);

        Assert.assertTrue(strings.length > 1);
    }

    @Test
    public void should_split_when_windows_line_ending(){
        String pattern = "(\\r\\n)|(\\n)";
        String text = "line1\r\nline2\r\nline3\r\n";

        String[] strings = text.split(pattern);

        Assert.assertTrue(strings.length > 1);
    }
}
