package pl.basecode.ocr.pattern.utils;

import java.math.BigDecimal;

public class TextComparer {
    public static boolean areEqual(String pattern, String text, boolean ignoreCase, boolean trim, boolean removeWhitespace) {
        pattern = prepareString(pattern, ignoreCase, trim, removeWhitespace);
        text = prepareString(text, ignoreCase, trim, removeWhitespace);

        return text.compareTo(pattern) == 0;
    }

    private static int getCorrectCharsCount(String pattern, String text, boolean ignoreCase, boolean trim, boolean removeWhitespace) {
        pattern = prepareString(pattern, ignoreCase, trim, removeWhitespace);
        text = prepareString(text, ignoreCase, trim, removeWhitespace);

        int correctCount = 0;

        for (int i = 0; i < pattern.length(); i++) {
            if (i >= text.length())
                break;

            if (pattern.charAt(i) == text.charAt(i))
                correctCount++;
        }

        return correctCount;
    }

    public static BigDecimal getResemblanceFactorForText(String[] patterns, String[] text, boolean ignoreCase, boolean trim, boolean removeWhitespace) {
        int patternsCharsCount = 0;
        int correctCount = 0;

        for (int i = 0; i < patterns.length; i++) {
            String pattern = prepareString(patterns[i], ignoreCase, trim, removeWhitespace);
            patternsCharsCount += pattern.length();

            if (i >= text.length)
                break;

            String textLine = text[i];
            correctCount += getCorrectCharsCount(pattern, textLine, ignoreCase, trim, removeWhitespace);
        }

        return new BigDecimal(((double) correctCount) / patternsCharsCount);
    }

    private static String prepareString(String data, boolean ignoreCase, boolean trim, boolean removeWhitespace) {
        if (trim) {
            data = data.trim();
        }

        if (removeWhitespace) {
            data = data.replaceAll("\\s", "");
        }

        if (ignoreCase) {
            data = data.toUpperCase();
        }

        return data;
    }

    public static BigDecimal getResemblanceFactorForLine(String pattern, String text, boolean ignoreCase, boolean trim, boolean removeWhitespace) {
        return new BigDecimal(((double) getCorrectCharsCount(pattern, text, ignoreCase, trim, removeWhitespace)) / pattern.length());
    }

    public static boolean areEqual(String pattern, String text) {
        return areEqual(pattern, text, false, false, false);
    }
}
