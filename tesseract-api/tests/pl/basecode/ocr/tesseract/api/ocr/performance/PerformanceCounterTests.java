/*
 * 
 */
package pl.basecode.ocr.tesseract.api.ocr.performance;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Bartosz Cłapa <bclapa@gmail.com>
 */
public class PerformanceCounterTests {
  final long returnValue = 5;
  
  @Test
  public void executes_action() throws Exception {
    ExecutionResult<Long> executionResult = execute();

    Assert.assertNotNull(executionResult);
  }

  @Test
  public void counts_time() throws Exception {
    ExecutionResult<Long> executionResult = execute();
    
    Assert.assertTrue(executionResult.executionTime > -1);
  }

  @Test
  public void returns_value() throws Exception {
    ExecutionResult<Long> executionResult = execute();
    
    Assert.assertTrue(executionResult.data == returnValue);
  }
  
  private ExecutionResult<Long> execute() throws Exception {
    return PerformanceCounter.execute(() -> doAction());
  }

  private long doAction() {
    return returnValue;
  }
}
