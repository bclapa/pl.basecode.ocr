/*
 * 
 */
package pl.basecode.ocr.tesseract.api.ocr.tesseract;

import org.junit.Ignore;
import pl.basecode.ocr.tesseract.api.ImageProviders.StreamImageProvider;
import java.awt.image.BufferedImage;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Bartosz Cłapa <bclapa@gmail.com>
 */
@Ignore
public class TesseractMechanismWrapperImplTests {
  
  @Test
  public void recognizes_any_characters(){
    String text = recognizeCharacters();    
    
    Assert.assertNotNull(text);
    Assert.assertNotEquals("", text);
  }
  
  @Test
  public void recognizes_specific_text(){
    String text = recognizeCharacters();    
    
    Assert.assertEquals("Zażółć gęślą jaźń", text.trim());
  }
  
  private String recognizeCharacters(){
    TesseractMechanismWrapper wrapper = new TesseractMechanismWrapperImpl();
    BufferedImage image = new StreamImageProvider().provideImage(this.getClass().getResourceAsStream("text_with_polish_letters.png"));
    String text = wrapper.recogniseCharacters(image);
    return text;
  }
}
