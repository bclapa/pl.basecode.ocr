package pl.basecode.ocr.tesseract.api.ocr.tesseract;

import org.junit.Test;

/**
 * Created by bclap on 20.06.2017.
 */
public class TesseractWrapperFileLoadTests {

  @Test(expected = Exception.class)
  public void throws_exception_when_not_image() throws Exception {
    throw new Exception();
  }

  @Test
  public void loads_file(){

  }

  @Test
  public void recognizes_text(){

  }

  @Test
  public void throws_exception_when_wrong_language_selected(){

  }
}
