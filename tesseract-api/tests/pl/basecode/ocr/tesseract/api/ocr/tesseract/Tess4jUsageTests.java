package pl.basecode.ocr.tesseract.api.ocr.tesseract;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.Assert;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import net.sourceforge.tess4j.ITesseract;

/**
 * Created by bclap on 21.06.2017.
 */
@Ignore
public class Tess4jUsageTests {

  @Test
  public void loads_polish_language_data_file() throws IOException {
    ITesseract tesseract = getTesseract("pol");
    
    BufferedImage image = ImageIO.read(this.getClass().getResourceAsStream("text_with_polish_letters.png"));

    TesseractException exceptionThrown = null;

    try {
      tesseract.doOCR(image);
    } catch (TesseractException ex) {
      exceptionThrown = ex;
    }

    Assert.assertEquals(null, exceptionThrown);
  }

  @Test
  public void recognizes_text() throws IOException, TesseractException {
    ITesseract tesseract = getTesseract("pol");

    BufferedImage image = ImageIO.read(this.getClass().getResourceAsStream("binarized.png"));

    String recognizedText = tesseract.doOCR(image);

    Assert.assertNotNull(recognizedText);

    recognizedText =recognizedText.trim(); // trimmed due to whitespaces on end of text

    Assert.assertEquals("Zażółć gęślą jaźń", recognizedText);
  }

  @Test
  public void throws_exception_on_missing_language_data() throws IOException, TesseractException {
    ITesseract tesseract = getTesseract("aaa");

    BufferedImage image = ImageIO.read(this.getClass().getResourceAsStream("text_with_polish_letters.png"));

    Error errorThrown = null;

    try {
      tesseract.doOCR(image);
    } catch (Error error) {
      errorThrown = error;
    }

    Assert.assertNotNull(errorThrown);
    Assert.assertEquals("Invalid memory access",errorThrown.getMessage());
  }

  private ITesseract getTesseract(String language) {
    ITesseract tesseract = new Tesseract();
//    File tessDataFolder = LoadLibs.extractTessResources("tessdata"); // Maven build bundles English data
//    tesseract.setDatapath(tessDataFolder.getParent());
    tesseract.setLanguage(language);

    return tesseract;
  }
}