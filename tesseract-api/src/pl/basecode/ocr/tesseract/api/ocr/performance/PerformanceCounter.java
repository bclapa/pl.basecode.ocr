/*
 * 
 */
package pl.basecode.ocr.tesseract.api.ocr.performance;

import java.util.concurrent.Callable;

/**
 * @author Bartosz Cłapa <bclapa@gmail.com>
 */
public final class PerformanceCounter {
    private PerformanceCounter(){

    }

    public static <T> ExecutionResult<T> execute(Callable<T> action) throws Exception {
        long startTime = System.nanoTime();

        T executionResult = action.call();

        long endTime = System.nanoTime();

        long executionTime = endTime - startTime;

        ExecutionResult<T> result;
        result = new ExecutionResult<>();
        result.setData(executionResult);
        result.setExecutionTime(executionTime);

        return result;
    }

    public static PerformanceResult execute(Runnable action) throws Exception {
        long startTime = System.nanoTime();

        action.run();

        long endTime = System.nanoTime();

        long executionTime = endTime - startTime;

        PerformanceResult result = new PerformanceResult();
        result.setExecutionTime(executionTime);

        return result;
    }
}