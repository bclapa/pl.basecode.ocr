/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.basecode.ocr.tesseract.api.ocr.tesseract;

import pl.basecode.ocr.tesseract.api.ocr.OcrMechanismWrapper;

/**
 *
 * @author Bartosz Cłapa <bclapa@gmail.com>
 */
public interface TesseractMechanismWrapper extends OcrMechanismWrapper {
  
}
