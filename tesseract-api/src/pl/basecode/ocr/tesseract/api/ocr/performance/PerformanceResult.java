/*
 * 
 */
package pl.basecode.ocr.tesseract.api.ocr.performance;

/**
 *
 * @author Bartosz Cłapa <bclapa@gmail.com>
 */
public class PerformanceResult {

  public long getExecutionTime() {
    return executionTime;
  }

  public void setExecutionTime(long executionTime) {
    this.executionTime = executionTime;
  }
  
  long executionTime;
}