/*
 * 
 */
package pl.basecode.ocr.tesseract.api.ocr.performance;

/**
 *
 * @author Bartosz Cłapa <bclapa@gmail.com>
 */
public class ExecutionResult<T> extends PerformanceResult{
    T data;    

    public T getData() {
      return data;
    }

    public void setData(T data) {
      this.data = data;
    }
  }