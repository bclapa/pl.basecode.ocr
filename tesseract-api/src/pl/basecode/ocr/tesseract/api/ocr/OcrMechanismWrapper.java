package pl.basecode.ocr.tesseract.api.ocr;

import java.awt.image.BufferedImage;

/**
 *
 * @author Bartek
 */
public interface OcrMechanismWrapper {
  String recogniseCharacters(BufferedImage image);
}
