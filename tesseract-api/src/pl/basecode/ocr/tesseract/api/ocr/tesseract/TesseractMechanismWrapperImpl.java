/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.basecode.ocr.tesseract.api.ocr.tesseract;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

public class TesseractMechanismWrapperImpl implements TesseractMechanismWrapper {

  @Override
  public String recogniseCharacters(BufferedImage image) {
    ITesseract tesseract = getTesseract("pol");

    String text = null;

    try {
      String recognizedChars = tesseract.doOCR(image);
      String lineEnding = recognizedChars.contains("\r\n") ? "\r\n" : "\n";
      String[] lines = recognizedChars.split("\\r?\\n");
      
      text = "";
      for (String line : lines){
        text += line.trim() + lineEnding;
      }
      
    } catch (TesseractException ex) {
      Logger.getLogger(TesseractMechanismWrapperImpl.class.getName()).log(Level.SEVERE, null, ex);
    }

    return text;
  }

  private ITesseract getTesseract(String language) {
    ITesseract tesseract = new Tesseract();
    File tessDataFolder = LoadLibs.extractTessResources("tessdata"); // Maven build bundles English data
    tesseract.setDatapath(tessDataFolder.getParent());
    tesseract.setLanguage(language);

    return tesseract;
  }
}
