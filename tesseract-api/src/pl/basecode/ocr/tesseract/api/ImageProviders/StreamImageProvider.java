package pl.basecode.ocr.tesseract.api.ImageProviders;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class StreamImageProvider implements ImageProvider {

  @Override
  public BufferedImage provideImage(InputStream imageStream) {
    try {
      return ImageIO.read(imageStream);
    } catch (IOException ex) {
      Logger.getLogger(StreamImageProvider.class.getName()).log(Level.SEVERE, null, ex);

      return null;
    }
  }

}
