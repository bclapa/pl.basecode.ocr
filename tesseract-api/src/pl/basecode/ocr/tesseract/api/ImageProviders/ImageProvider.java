package pl.basecode.ocr.tesseract.api.ImageProviders;

import java.awt.image.BufferedImage;
import java.io.InputStream;

/*
 * Created by bclap on 21.06.2017.
 */
public interface ImageProvider {
  BufferedImage provideImage(InputStream imageStream);
}