package pl.basecode.ocr.tesseract.app;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SummaryResult{
    private List<String> recognizedTexts;
    private String properText;
    private long totalExecutionTime;
    private int executionCount;
    private BigDecimal sumOfResemblanceFactor;

    public SummaryResult() {
        recognizedTexts = new ArrayList<>();
        totalExecutionTime = 0;
        sumOfResemblanceFactor = BigDecimal.ZERO;
    }

    public void addText(String text){
        recognizedTexts.add(text);
    }

    public String getProperText() {
        return properText;
    }

    public void setProperText(String properText) {
        this.properText = properText;
    }

    public long getTotalExecutionTime() {
        return totalExecutionTime;
    }

    public void setTotalExecutionTime(long totalExecutionTime) {
        this.totalExecutionTime = totalExecutionTime;
    }

    public int getExecutionCount() {
        return executionCount;
    }

    public void setExecutionCount(int executionCount) {
        this.executionCount = executionCount;
    }

    public void addExecutionTime(long executionTime) {
        totalExecutionTime += executionTime;
    }

    public void addResemblanceFactor(BigDecimal resemblanceFactor){
        sumOfResemblanceFactor=sumOfResemblanceFactor.add(resemblanceFactor);
    }

    public BigDecimal getSumOfResemblanceFactor() {
        return sumOfResemblanceFactor;
    }
}
