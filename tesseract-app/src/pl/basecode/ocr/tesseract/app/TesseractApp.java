package pl.basecode.ocr.tesseract.app;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import pl.basecode.ocr.pattern.utils.TextComparer;
import pl.basecode.ocr.tesseract.api.ImageProviders.ImageProvider;
import pl.basecode.ocr.tesseract.api.ImageProviders.StreamImageProvider;
import pl.basecode.ocr.tesseract.api.ocr.performance.ExecutionResult;
import pl.basecode.ocr.tesseract.api.ocr.performance.PerformanceCounter;
import pl.basecode.ocr.tesseract.api.ocr.tesseract.TesseractMechanismWrapper;
import pl.basecode.ocr.tesseract.api.ocr.tesseract.TesseractMechanismWrapperImpl;
import pl.basecode.ocr.tesseract.app.configuration.ConfigEntry;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class TesseractApp {
    public static void main(String[] args) throws Exception {
        if (args.length != 1) return;

        File xmlFile = new File(args[0]);
        SAXBuilder builder = new SAXBuilder();

        Document doc = builder.build(xmlFile);

        List<ConfigEntry> entries = new ArrayList<>();

        for (Element child : doc.getRootElement().getChildren()) {
            ConfigEntry entry = new ConfigEntry();
            entry.setImagePath(child.getChildText("imagePath"));
            entry.setText(child.getChildText("text"));

            entries.add(entry);
        }

        ImageProvider imgProvider = new StreamImageProvider();
        TesseractMechanismWrapper wrapper = new TesseractMechanismWrapperImpl();

        List<SummaryResult> summaryResults = new ArrayList<>();
        String regexPattern = "(\\r\\n)|(\\n)";

        BigDecimal nanoToSecondFactor = BigDecimal.valueOf(0.1).pow(9);
        int runCount = 100;
        for (ConfigEntry entry : entries) {
            InputStream stream = new FileInputStream(entry.getImagePath());
            BufferedImage img = imgProvider.provideImage(stream);
            SummaryResult sr = new SummaryResult();

            sr.setProperText(entry.getText());
            sr.setExecutionCount(runCount);

            String[] multilineText = entry.getText().split(regexPattern);

            for (int i = 0; i < runCount; i++) {
                ExecutionResult<String> result = PerformanceCounter.execute(() -> wrapper.recogniseCharacters(img));

                sr.addExecutionTime(result.getExecutionTime());
                sr.addText(result.getData());

                BigDecimal resemblanceFactorForText = TextComparer.getResemblanceFactorForText(multilineText, result.getData().split(regexPattern), false, true, false);
                sr.addResemblanceFactor(resemblanceFactorForText);
            }

            summaryResults.add(sr);

            System.out.print("Execution time: ");
            System.out.println(BigDecimal.valueOf(sr.getTotalExecutionTime()).divide(BigDecimal.valueOf(sr.getExecutionCount()),5, RoundingMode.HALF_UP).multiply(nanoToSecondFactor));

            System.out.print("Average resemblance factor: ");
            System.out.println(sr.getSumOfResemblanceFactor().divide(BigDecimal.valueOf(sr.getExecutionCount()),5, RoundingMode.HALF_UP));
        }
    }
}